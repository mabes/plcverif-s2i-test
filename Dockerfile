# A FROM line must be present but is ignored. It will be overridden by the --image-stream parameter in the BuildConfig
FROM registry.access.redhat.com/ubi8/nodejs-14

# Temporarily switch to root user to install packages
USER root

# Install java
RUN dnf install -y java-11-openjdk.x86_64

# Make sure the final image runs as unprivileged user
USER 1001
