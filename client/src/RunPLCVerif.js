import React, { Component } from 'react';
import axios from 'axios';

class RunPLCVerif extends Component {
    constructor(props) {
        super(props);

        this.state = {
            code: '',
            rules: ''
        };
    }
    
    handleInputChange = e => {
        this.setState({
            [e.target.name]: e.target.value,
        });
    };
    
    handleSubmit = e => {
        e.preventDefault();
        const { code, rules } = this.state;
        
        const cliParams = {
            code,
            rules
        };
        
        axios
            .post('/run', cliParams)
            .then(() => console.log('Posted ' + cliParams))
            .catch(err => {
                console.error(err);
            });
    };
    
    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <div className='rowC'>
                    <div className="codeBox">
                        <label>Code: </label><br />
                        <textarea
                            className="codeBox"
                            name="code"
                            onChange={this.handleInputChange}
                        />
                    </div>
                    <div className="codeBox">
                        <label>Rules: </label><br />
                        <textarea
                            className="codeBox"
                            name="rules"
                            onChange={this.handleInputChange}
                        />
                    </div>
                </div>

                <div>
                    <button className="btn btn-success" type="submit">RUN</button>
                </div>
            </form>
        );
    };
}

export default RunPLCVerif;
