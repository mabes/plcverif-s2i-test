import React from "react";
import axios from 'axios';
import logo from './logo.svg';
import './App.css'; 
import RunPLCVerif from './RunPLCVerif.js';

function App() {
    
    return (
        <div className="App">
            <header className="App-header">
                <p>PLCVerif</p>
            </header>
            <RunPLCVerif />
        </div>
  );
}

export default App;
