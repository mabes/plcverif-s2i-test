const path = require('path');
const express = require('express');
const { exec } = require('child_process');

const PORT = 8080;

const app = express();

// Have Node serve the files for our built React app
app.use(express.static(path.resolve(__dirname, 'client/build')));
app.use(express.json());

app.get("/api", (req, res) => {
  res.json({ message: "Hello from server!" });
});

// All other GET requests not handled before will return our React app
app.get('*', (req, res) => {
  res.sendFile(path.resolve(__dirname, 'client/build', 'index.html'));
});

app.post('/run', function(req, res) {
	console.log(req.body.code);
    console.log(req.body.rules);
        
    exec('java --version', (error, stdout, stderr) => {
        if (error) {
            console.error(`exec error: ${error}`);
            return;
        }
    });
});

app.listen(PORT, () => {
  console.log(`Server running on port: ${PORT}`);
});
